def mtq_saturation(magnetic_dipole):
    """
        Function for desaturating the MTQs in case that the induced magnetic
        dipole exceeds the available maximum dipole provided by each
        magnetorquer
    """
    for i in range(0, 3):
        if magnetic_dipole[i] > 0.4:
            magnetic_dipole[i] = 0.4
        elif magnetic_dipole[i] < -0.4:
            magnetic_dipole[i] = -0.4

    return magnetic_dipole
