import numpy as np
from workshop_functions import *

class EKF:
    """
        Class that models the Extended Kalman Filter
    """

    def __init__(self, stateTransFun_ptr, stateTransFunJacobian_ptr, measurementFun_ptr, measurementFunJacobian_ptr, inertia_matrix, timestep):
        """
            Class constructor that takes as input pointers to the state transition, state transition Jacobian, measurements and measurements Jacobian
            functions, satellite's inertia matrix and derivation timestep
        """
        self.stateTransFunJacobian_ptr = stateTransFunJacobian_ptr
        self.stateTransFun_ptr = stateTransFun_ptr
        self.measurementFun_ptr = measurementFun_ptr
        self.measurementFunJacobian_ptr = measurementFunJacobian_ptr

        self.state_transition_Jacobian = np.array([[0 for i in range(7)] for j in range(7)], dtype='d')
        self.measurements_Jacobian = np.array([[0 for i in range(9)] for j in range(7)], dtype='d')

        self.Kalman_Gain = np.array([[0 for i in range(9)] for j in range(7)], dtype='d')
        self.process_noise_covariance = np.diag(np.array([1, 1, 1, 1, 0.01, 0.01, 0.01], dtype='d'))
        self.measurement_noise_covariance = np.diag(np.array([20, 20, 20, 0.01, 0.01, 0.01, 20, 20, 20], dtype='d'))*1e-04
        self.covariance_matrix = np.identity(7)

        self.state_estimation = np.array([0.5, -0.5, 0.5, -0.5, 0.02, 0.02, 0.02], dtype='d')

        self.dx = np.array([0.001 for i in range(7)], dtype='d')
        self.dy = np.array([0.001 for i in range(9)], dtype='d')

        self.inertia_matrix = inertia_matrix
        self.timestep = timestep

    def predict(self, torque):
        """
            Extended Kalman Filter predict function which produces a state estimation
        """
        self.state_transition_Jacobian = self.stateTransFunJacobian_ptr(self.state_estimation, self.dx)
        self.state_estimation = self.stateTransFun_ptr(self.state_estimation, torque, self.timestep)

        self.state_estimation[0:4] = self.state_estimation[0:4]/np.linalg.norm(self.state_estimation[0:4])
        self.covariance_matrix = np.matmul(np.matmul(self.state_transition_Jacobian, self.covariance_matrix), self.state_transition_Jacobian.transpose()) + self.process_noise_covariance


    def correct(self, magnetic_field, sun_pos, sensors_measurements):
        """
            Extended Kalman filter correct function which corrects the state estimation based on the groundtruth measurements
        """
        measurements_estimation = self.measurementFun_ptr(self.state_estimation, magnetic_field, sun_pos)
        self.measurements_Jacobian = self.measurementFunJacobian_ptr(measurements_estimation, self.dy)

        self.Kalman_Gain = np.matmul(np.matmul(self.covariance_matrix, self.measurements_Jacobian.transpose()), np.linalg.inv(np.matmul(np.matmul(self.measurements_Jacobian, self.covariance_matrix), self.measurements_Jacobian.transpose()) + self.measurement_noise_covariance))

        self.state_estimation = self.state_estimation + np.matmul(self.Kalman_Gain, sensors_measurements - measurements_estimation)
        self.state_estimation[0:4] = self.state_estimation[0:4]/np.linalg.norm(self.state_estimation[0:4])
        self.covariance_matrix = np.matmul(np.matmul(np.identity(7) - (np.matmul(self.Kalman_Gain, self.measurements_Jacobian)), self.covariance_matrix), (np.identity(7) - np.matmul(self.Kalman_Gain, self.measurements_Jacobian)).transpose()) + np.matmul(np.matmul(self.Kalman_Gain, self.measurement_noise_covariance), self.Kalman_Gain.transpose())


