import scipy.io
import math
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from quaternions import *
from workshop_functions import *

matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['font.family'] = 'STIXGeneral'


# Magnetic field values in ECI frame
mag_field_eci = scipy.io.loadmat('orbit propagator matrices/mag_field_eci.mat')
mag_field_eci = np.asarray(mag_field_eci['mag_field_eci']*10**(-9))

Time = 5000  #Simulation run time in sec

# Matrices initialization
torque = np.array([0, 0, 0], dtype='d')
Bdot = np.array([0, 0, 0], dtype='d')
magnetic_dipole = np.array([0, 0, 0], dtype='d')
magnetic_field_body = np.array([0, 0, 0], dtype='d')
magnetic_field_body_prev = np.array([0, 0, 0], dtype='d')
angular_velocity_data = np.empty((3, Time))
cycle_time = []
t = 0


# Initial state
initial_quaternion = np.array([0.5, -0.5, 0.5, -0.5], dtype='d')
initial_angular_velocity = np.array([0.5, 0.5, 0.3], dtype='d')
state = np.array([0 for i in range(7)], dtype='d')
state[0:4] = initial_quaternion
state[4:7] = initial_angular_velocity


# Main loop
while t < Time:

    for current_timestep in range(0, 10):
        
    # Magnetometer measurment
        magnetic_field_body = vector_quaternion_turn(mag_field_eci[:, 10*t+current_timestep], state[0:4])
    # Bdot calculation
        Bdot = (magnetic_field_body - magnetic_field_body_prev)/0.1
    # Bdot controller function
        torque = Detumbling_Controller(Bdot, magnetic_field_body, state[4:7])
    # Propagate the state
        state = stateTransFun(state, torque, 0.1)
        
    # Magnetometer previous measurment update
        magnetic_field_body_prev = magnetic_field_body
        
    # Matrices update
    angular_velocity_data[:, t] = state[4:7]
    cycle_time.append(t)    
    t += 1



# Plots
fig, ax = plt.subplots(3)
fig.suptitle('Angular Velocity [rad/s]')
plt.xlabel('Time [s]')
for i in range(3):
    ax[i].plot(cycle_time, angular_velocity_data[i])
    ax[i].grid()
ax[0].legend(['$\omega_{x}$'], loc='upper right')
ax[1].legend(['$\omega_{y}$'], loc='upper right')
ax[2].legend(['$\omega_{z}$'], loc='upper right')

plt.show()
