import numpy as np
from quaternions import *
import matplotlib.pyplot as plt
from system_model import *
from disturbances import *
import scipy.io
import matplotlib
from workshop_functions import *
from EKF import *
import random

matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['font.family'] = 'STIXGeneral'

## Magnetic field values
mag_field_orbit = scipy.io.loadmat('orbit propagator matrices/mag_field_orbit.mat')
mag_field_orbit = np.asarray(mag_field_orbit['mag_field_orbit'] * 10 ** (-9))

## Sun position values
sun_pos_orbit = scipy.io.loadmat('orbit propagator matrices/sun_pos_orbit.mat')
sun_pos_orbit = np.asarray(sun_pos_orbit['sun_pos_orbit'])

## Initial Satellite State
q0 = np.array([0.5, -0.5, 0.5, -0.5])
v0 = np.array([0.02, 0.02, 0.02])
state = np.concatenate([q0, v0])

## Simulation Parameters
inertia_matrix = np.array([[0.035, 0, 0], [0, 0.035, 0], [0, 0, 0.006]])
timestep = 0.1
Seconds = 800
Kp = np.array([[0.0012, 0, 0], [0, 0.0012, 0], [0, 0, 0.0012]])
Kd = np.array([[0.001, 0, 0], [0, 0.001, 0], [0, 0, 0.001]])
q_desired = np.array([1, 0, 0, 0])

## Matrices initialization
q_data = np.empty((4, Seconds))
Angular_velocity_data = np.empty((3, Seconds))
state_estimation_data = np.empty((4, Seconds))
angular_velocity_estimation_data = np.empty((3, Seconds))
Time = []
T_commanded = np.array([0, 0, 0], dtype='d')
Current_Timestep = 0
stateTransFun_ptr = stateTransFun
stateTransFunJacobian_ptr = stateTransFunJacobian
measurementFun_ptr = measurementsFun
measurementFunJacobian_ptr = measurementFunJacobian
timestep = 1

ekf = EKF(stateTransFun_ptr, stateTransFunJacobian_ptr, measurementFun_ptr, measurementFunJacobian_ptr, inertia_matrix,
          timestep=timestep)

# Main loop

while Current_Timestep < Seconds:
    sensors_measurements = measurementsFun(state, mag_field_orbit[:, 10 * Current_Timestep],
                                           sun_pos_orbit[:, 10 * Current_Timestep])

    # Measurement noise  gauss(mean,std_dev)

    sensors_measurements[0:3] += random.gauss(0, 0.01)  # Magnetic field
    sensors_measurements[3:6] += random.gauss(0, 0.001)  # Gyroscope
    sensors_measurements[6:8] += random.gauss(0, 0.01)  # Sun sensor

    ekf.correct(mag_field_orbit[:, 10 * Current_Timestep], sun_pos_orbit[:, 10 * Current_Timestep],
                sensors_measurements)

    # Controller function
    T_commanded = PD_Controller(ekf.state_estimation[0:4], ekf.state_estimation[4:7])
    # Disturbances
    T_disturbances = disturbances(mag_field_orbit[:, 10 * Current_Timestep], state[0:4])
    # Propagate the state
    state = stateTransFun(state, torque=T_commanded + T_disturbances, timestep=1)

    ekf.predict(T_commanded + T_disturbances)

    Time.append(Current_Timestep)

    q_data[:, Current_Timestep] = state[0:4]
    Angular_velocity_data[:, Current_Timestep] = state[4:7]
    state_estimation_data[:, Current_Timestep] = ekf.state_estimation[0:4] - state[0:4]
    angular_velocity_estimation_data[:, Current_Timestep] = ekf.state_estimation[4:7] - state[4:7]
    Current_Timestep += 1

fig, ax = plt.subplots(4)
fig.suptitle('Quaternion estimation error')
plt.xlabel('Time [s]')
for i in range(4):
    ax[i].plot(Time, state_estimation_data[i])
    ax[i].grid()
    ax[i].legend(['$\^q_{}$'.format(i + 1)], loc='upper right')

fig, ax = plt.subplots(3)
fig.suptitle('Angular Velocity estimation error[rad/s]')
plt.xlabel('Time [s]')
for i in range(3):
    ax[i].plot(Time, angular_velocity_estimation_data[i])
    ax[i].grid()
ax[0].legend(['$\^\omega_{x}$'], loc='upper right')
ax[1].legend(['$\^\omega_{y}$'], loc='upper right')
ax[2].legend(['$\^\omega_{z}$'], loc='upper right')

fig, ax = plt.subplots(4)
fig.suptitle('Quaternion')
plt.xlabel('Time [s]')
for i in range(4):
    ax[i].plot(Time, q_data[i])
    ax[i].grid()
    ax[i].legend(['$q_{}$'.format(i + 1)], loc='upper right')

fig, ax = plt.subplots(3)
fig.suptitle('Angular Velocity [rad/s]')
plt.xlabel('Time [s]')
for i in range(3):
    ax[i].plot(Time, Angular_velocity_data[i])
    ax[i].grid()
ax[0].legend(['$\omega_{x}$'], loc='upper right')
ax[1].legend(['$\omega_{y}$'], loc='upper right')
ax[2].legend(['$\omega_{z}$'], loc='upper right')

plt.show()
