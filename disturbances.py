import numpy as np
from quaternions import vector_quaternion_turn

def disturbances(mag_field_orbit, quaternion):
    '''
        Function that models and returns the most significant disturbances torques (residual magnetic dipole and gravitational torque).  
    '''
    inertia_matrix = np.array([[0.035, 0, 0], [0, 0.035, 0], [0, 0, 0.006]], dtype='d')
    
    orbit_angular_vel = 0.0011
    
    r00 = 2 * (quaternion[0] * quaternion[0] + quaternion[1] * quaternion[1]) - 1
    r01 = 2 * (quaternion[1] * quaternion[2] - quaternion[0] * quaternion[3])
    r02 = 2 * (quaternion[1] * quaternion[3] + quaternion[0] * quaternion[2])

    nadir = np.array([r00, r01, r02], dtype='d')
    T_gravity = 3 * (orbit_angular_vel ** 2) * np.cross(nadir, np.matmul(inertia_matrix, nadir))

    Residual_dipole = np.array([0.01, 0.01, 0.01], dtype='d')
    magnetic_field_body = vector_quaternion_turn(mag_field_orbit, quaternion)
    T_magnetic = np.cross(Residual_dipole, magnetic_field_body)

    T_disturbances = T_magnetic + T_gravity

    return T_disturbances
