import scipy.io
import numpy as np
from quaternions import *
import matplotlib.pyplot as plt
from system_model import *
from disturbances import *
import matplotlib
from workshop_functions import *
import scipy.io

## Magnetic field values in orbit frame
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['font.family'] = 'STIXGeneral'

mag_field_orbit = scipy.io.loadmat('orbit propagator matrices/mag_field_orbit.mat')
mag_field_orbit = np.asarray(mag_field_orbit['mag_field_orbit'] * 10 ** (-9))

## Initial Satellite State
q0 = np.array([0.5, -0.5, 0.5, -0.5])
v0 = np.array([0.03, 0.03, 0.03])
state = np.concatenate([q0, v0])

## Simulation Parameters
Seconds = 2000

## Matrices initialization
q_data = np.empty((4, Seconds))
Angular_velocity_data = np.empty((3, Seconds))
Time = []
T_commanded = np.array([0, 0, 0])
Current_Timestep = 0

# Main loop

while Current_Timestep < Seconds:

# Controller function
    T_commanded = PD_Controller(state[0:4], state[4:7])
# Disturbances
    T_disturbances = disturbances(mag_field_orbit[:, 10 * Current_Timestep], state[0:4])
# Propagate the state
    state = stateTransFun(state, torque=T_commanded + T_disturbances, timestep=1)
# Matrices update
    Time.append(Current_Timestep)
    q_data[:, Current_Timestep] = state[0:4]
    Angular_velocity_data[:, Current_Timestep] = state[4:7]

    Current_Timestep += 1

fig, ax = plt.subplots(4)
fig.suptitle('Quaternion')
plt.xlabel('Time [s]')
for i in range(4):
    ax[i].plot(Time, q_data[i])
    ax[i].grid()
    ax[i].legend(['$q_{}$'.format(i+1)], loc='upper right')

fig, ax = plt.subplots(3)
fig.suptitle('Angular Velocity [rad/s]')
plt.xlabel('Time [s]')
for i in range(3):
    ax[i].plot(Time, Angular_velocity_data[i])
    ax[i].grid()
ax[0].legend(['$\omega_{x}$'], loc='upper right')
ax[1].legend(['$\omega_{y}$'], loc='upper right')
ax[2].legend(['$\omega_{z}$'], loc='upper right')

plt.show()
