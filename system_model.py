import numpy as np
from quaternions import *

def measurementsFun(state, magnetic_field, sun_pos):
    """
        Gets as input the state (7-element vector consisting of the quaternion representation of the satellite's orientation and the angular velocity), 
        the magnetic field and the sun position vectors and performs the needed rotations so that the aforementioned inputs are expressed in the body frame.
        
    """
    quaternion = state[0:4]
    angular_velocity = state[4:7]
    measurements = np.array([0 for i in range(9)], dtype='d')

    magn_field = vector_quaternion_turn(magnetic_field, quaternion)
    measurements[0:3] = magn_field/np.linalg.norm(magn_field)

    measurements[3:6] = angular_velocity

    sun_pos = vector_quaternion_turn(sun_pos, quaternion)
    measurements[6:9] = sun_pos/np.linalg.norm(sun_pos)

    return measurements

def stateTransFunJacobian(state, dx):
    """
        Calculates the Jacobian matrix of the state 
    """
    Jacobian1 = state - dx
    Jacobian2 = state + dx
    Jacobian = np.array([[0 for i in range(7)] for j in range(7)], dtype='d')

    for i in range(0, 7):
        for j in range(0, 7):
            Jacobian[i][j] = (Jacobian2[j] - Jacobian1[j])/(2*dx[j])

    return Jacobian

def measurementFunJacobian(measurements, dy):
    """
        Calculates the Jacobian matrix of the measurements 
    """
    Jacobian1 = measurements - dy
    Jacobian2 = measurements + dy
    Jacobian = np.array([[0 for i in range(7)] for j in range(9)], dtype='d')

    for i in range(0, 9):
        for j in range(0, 7):
            Jacobian[i][j] = (Jacobian2[i] - Jacobian1[i])/(2*dy[i])

    return Jacobian
